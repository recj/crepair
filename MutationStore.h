/*********************************************************************** 
 * MutationStore.h
 * Author: Rod Eric Joseph
 * Description: Specifies a MutationStore, which stores the list of
 * possible mutations of a program.
 **********************************************************************/

using namespace llvm;

class MutationStore
{
public:
  typedef Mutation pair <iterator_range<use_iterator>, BinaryOps;
  
  /* Stores a (key, value) pair containing uses of instruction and the
     opcode to replace with for each use*/
  vector <Mutation> members;

  /* Sets of n mutations to try to apply in order of number of mutations
     (and priority?) */
  vector <Mutation> combinations(int n);

  /* Add a key, value pair to the structure */
  void add(iterator_range<use_iterator> *uses, BinaryOps *opcode);

  /* Apply the mutation to the program, replacing op codes as
     appropriate. */
  void apply(iterator_range<use_iterator> *uses, BinaryOps *opcode);
}; 
