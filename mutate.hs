#!/usr/bin/env runhaskell

{- |
Module      :  mutate.hs
Description :  A script to apply mutations to the program, repaiting it, and
               pass them to the verifier.
Copyright   :  (c) Rod Eric Joseph
License     :  GNU General Public License v3.0

Maintainer  :  rejoseph@princeton.edu
Stability   :  experimental
Portability :  portable

Enumerates the different operators that can be replaced.
-}

import System.IO
import System.Environment
import System.Process
import Text.Printf
import System.Exit

data BinaryOp
  = PLUS
  | MINUS
  | AND
  | OR
  | SHIFTL
  | SHIFTR
  | EQEQ
  | MUL
  | DIV
  | MOD
  deriving Enum

data UnaryOp
  = INC
  | DEC
  deriving Enum

data AssignmentOp
  = PLUSEQ
  | MINUSEQ
  deriving Enum

data Operation
  = Data String
  | BinOp BinaryOp
  | UnOp UnaryOp
  | AssnOp AssignmentOp

-- Define how binary operators apear in the program.
instance Show BinaryOp where
  show PLUS
    = "+"
  show MINUS
    = "-"
  show AND
    = "&&"
  show OR
    = "||"
  show SHIFTL
    = "<<"
  show SHIFTR
    = ">>"
  show EQEQ
    = "=="
  show MUL
    = "*"
  show DIV
    = "/"
  show MOD
    = "%"

instance Show UnaryOp where
  show INC
    = "++"
  show DEC
    = "--"

instance Show AssignmentOp where
  show PLUSEQ
    = "+="
  show MINUSEQ
    = "-="

instance Show Operation where
  show (Data x)
    = x
  show (BinOp x)
    = show x
  show (UnOp x)
    = show x
  show (AssnOp x)
    = show x

-- Define lexical analysis of binary operations in character sequences.
lexer :: String
      -> [Operation]
lexer ('+':'+':xs)
  = (UnOp INC):(lexer xs)
lexer ('+':'=':xs)
  = (AssnOp PLUSEQ):(lexer xs)
lexer ('+':xs)
  = (BinOp PLUS):(lexer xs)
lexer ('-':'-':xs)
  = (UnOp DEC):(lexer xs)
lexer ('-':'=':xs)
  = (AssnOp MINUSEQ):(lexer xs)
lexer ('-':xs)
  = (BinOp MINUS):(lexer xs)
lexer ('=':'=':xs)
  = (BinOp EQEQ):(lexer xs)
lexer ('>':'>':xs)
  = (BinOp SHIFTR):(lexer xs)
lexer ('<':'<':xs)
  = (BinOp SHIFTL):(lexer xs)
lexer ('&':'&':xs)
  = (BinOp AND):(lexer xs)
lexer ('|':'|':xs)
  = (BinOp OR):(lexer xs)
lexer ('*':xs)
  = (BinOp MUL):(lexer xs)
lexer ('/':xs)
  = (BinOp DIV):(lexer xs)
lexer ('%':xs)
  = (BinOp MOD):(lexer xs)
lexer (x:xs)
  = (Data [x]):(lexer xs)
lexer []
  = []

-- make all possible combinations of mutations
mkallpos :: [Operation]
    -> [[Operation]]
mkallpos ((Data _):xs)
  = mkallpos xs
mkallpos ((BinOp x):xs)
  = do y  <- enumFrom ((toEnum 0) :: BinaryOp)
       ys <- mkallpos xs
       return ((BinOp y):ys)
mkallpos ((UnOp x):xs)
  = do y  <- enumFrom ((toEnum 0) :: UnaryOp)
       ys <- mkallpos xs
       return ((UnOp y):ys)
mkallpos ((AssnOp x):xs)
  = do y  <- enumFrom ((toEnum 0) :: AssignmentOp)
       ys <- mkallpos xs
       return ((AssnOp y):ys)
mkallpos []
  = [[]]

-- calls seahorn verifier on mutated program
callVerifier :: IO Bool
callVerifier
  = do (e, s, _) <- readProcessWithExitCode "sea" ["pf", "testfile.c"] ""
       return $ case e of
                  ExitSuccess -> "unsat" == last (lines s)
                  _           -> False


loopy :: Handle
      -> [Operation]
      -> [Operation]
      -> IO ()
loopy h ((Data x):xs) mut
  = do hPutStr h x
       loopy h xs mut
loopy h (_:xs) (y:ys)
  = do hPutStr h (show y)
       loopy h xs ys
loopy h []     []
  = return ()

try :: [Operation]
    -> [Operation]
    -> IO Bool
    
-- Write mutated program to new testfile.c
try ops mut
  = do h <- openFile "testfile.c" WriteMode
       loopy h ops mut
       hClose h
       callVerifier

-- Tries all sets of mutations, printing if solution found
try_all :: [Operation]
       -> [[Operation]]
       -> Int
       -> IO ()
try_all ops (x:xs) i
  = do a <- try ops x
       case a of
         True  -> printf "OK!!! (%d sets found, %d set(s) tried)\n" (length xs +i+1) (i+1)
         False -> try_all ops xs (i+1)
try_all _ [] _
  = putStrLn "fail (no solution found)"

-- Mutatate, repair, and verify input program.
run :: String -> IO ()
run s
  = let lst = lexer s
    in
      tryAll lst (mkallpos lst) 0

main_loop :: Bool
          -> [String]
          -> IO ()
main_loop True []
  = do s <- getContents
       run s
main_loop _ (x:xs)
  = do s <- readFile x
       run s
       main_loop False xs
main_loop False [] = return ()

main :: IO ()
main
  = do args <- getArgs
       main_loop True args
