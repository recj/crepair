# Use seahorn image
FROM seahorn/seahorn

# Set working directory to /crepair
WORKDIR /crepair

# Install packages necessary for dev environment
RUN apt-get install emacs runhaskell
