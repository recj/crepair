/***********************************************************************
 * MutationPass.cpp
 * Author: Rod Eric Joseph
 * Description: An LLVM pass to replace binary operators when making 
 * mutations.
 ***********************************************************************/

#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/IR/User.h"
#include "llvm/IR/Module.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Bitcode/ReaderWriter.h"
#include "llvm/Support/FileSystem.h"
#include "MutationStore.h"

#define DEBUG_TYPE "MutationPass"

using namespace llvm;
/* Adapted from https://github.com/sampsyo/llvm-pass-skeleton/tree/mutate */
/*
  Cases to handle:
  Binary operators:
  | Add
  | Subtract
  | Multiply
  | Divide
  | And
  | Or

  Variables:
  | (int) variable+1
  | (int) variable-1

*/
namespace {
  struct MutationPass : public FunctionPass {
    static char ID;
    MutationPass() : FunctionPass(ID) {}

    // return value: whether or not code was modified
    virtual bool runOnFunction(Function &F) {
      
      mutation_store = new MutationStore();
      
      for (auto &B : F) {
        for (auto &I : B) {
	  // How to identify binary operator itself?
          if (auto *op = dyn_cast<BinaryOperator>(&I)) {
            // Insert at the point where the instruction `op` appears.
            IRBuilder<> builder(op);
	    Value *new_op;

	    // Match different instructions to the opcodes that could
	    // them 
	    switch(op.getopcode()) {
	    case llvm::Instruction::Add:
	      // Subtraction - make a addition with the same operands as `op`.
	      new_op = builder.CreateSub(lhs, rhs);
	      break;
		
	    case llvm::Instruction::Sub:
	      // Subtraction - make a addition with the same operands as `op`.
	      new_op = builder.CreateAdd(lhs, rhs);
	      break;
	    }
	    
	    // Add new mutation to mutation store if there is one.
	    if (new_op != NULL)
	      {
		Value *lhs = op->getOperand(0);
	        Value *rhs = op->getOperand(1);
		MutationStore.add(op->uses(), new_op);
	      }
          }
        }
	// For each mutation in the MutationStore...
	for(int i=0; i < mutation_store.members.size(); i++)
	  {
	    //Write the mutation to a file.
	  }
      }
      return true;
    }
  };
}

char MutationPass::ID = 0;

// Automatically enable the pass.
// http://adriansampson.net/blog/clangpass.html
static void registerMutationPass(const PassManagerBuilder &,
				 legacy::PassManagerBase &PM) {
  PM.add(new MutationPass());
}
static RegisterStandardPasses
RegisterMyPass(PassManagerBuilder::EP_EarlyAsPossible,
	       registerMutationPass);
