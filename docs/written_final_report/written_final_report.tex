\documentclass[pageno]{jpaper}

\newcommand{\IWreport}{Spring 2019}
\newcommand{\quotes}[1]{``#1''}


\widowpenalty=9999

\usepackage[normalem]{ulem}
\usepackage{listings} %For code in appendix
\lstset{
  inputencoding=utf8,
  extendedchars=true,
  mathescape=true,
  texcl=true,
  basicstyle=\footnotesize\ttfamily,
  showstringspaces=false,
  showspaces=false,
  numbers=left,
  numberstyle=\footnotesize,
  numbersep=9pt,
  tabsize=8,
  breaklines=true,
  showtabs=false,
  captionpos=b,
  emph={data, where, induct, let, case, match, Set, lambda, forall, deriving, import, fix, impl, macro, Type, Level, ZLevel, SLevel},
  emphstyle={\color{blue}\bfseries},
  literate={∀}{{$\forall$}}1 {→}{{$\to$}}1
}
\begin{document}

\title{
Automated C Program Repair}

\author{Rod Eric Joseph\\Adviser: Aarti Gupta}

\date{May 6, 2019}
\maketitle

\thispagestyle{empty}
\doublespacing
\begin{abstract}
This paper describes the design and implementation of a tool to automatically
repair C programs given a set of assertions that should be met. The paper
discusses some of the previous approaches and how the approach they suggest can
be used in a tool that focuses on eliminating some of the most common and simple
bugs in C programs. It then delves into its unique approach, how
repair was implemented, an analysis of the limitations and results of
the tool, and work that could be done in the future to improve such
tools using a similar approach.


\end{abstract}

\section{Introduction}
% Introduction
There are are very few things that can be safely said with absolute certainty,
but the fact that having a bug in a program is not a pleasant experience
for programmers is definitely one of them. Even the simplest bugs can
be incredibly difficult to detect, sometimes costing hours of time for
programmers. Though with more experience, programmers often learn to
look out for certain bugs and use different methods to debug their
programs, for example, using print statements and break points, and
debuggers such as GDB, such manual methods still require effort and
time on the programmer. Moreover, once the bug is found, it is very
rare for it to be simple to fix. In fact, it is often said that once
the bug is found is when the work to remove errors in the program
truly begins. However, because there are reasonable
limitations on the number of expressions that can replace a buggy
section in a program, it is worth looking into an
automated method of eliminating these bugs to speed up the early
stages of development, increase morale of developers, and ultimately
help programmers focus on high-level concept rather than low-level
minute bugs that get in the way of correct behavior of programs.

\subsection{Motivation and Goal}
%Motivation
Though it is an incredibly powerful language that has a wide of
applications in creating systems software, embedded software,
high-speed software, and many other realms where performance and
access to low-level system infrastructure may be important,
debugging C programs can be especially jarring, especially to beginners to the
language. The lack of safety in many aspects of the language, for example, the
accessibility of pointer arithmetic and implicit casts allow for a variety of
pitfalls in the language. However, some of these bugs can be relatively simple,
and it is often some of the simplest bugs, such as using the wrong arithmetic
operation, off by one errors, and failure to increment, that can cause
trouble in the early stages of development. Therefore, though these
bugs are simple and cannot encompass the entire scope of bugs in
large software, they constitute a disproportionately large amount of bugs in
programs, making them a good candidate for a subset of bugs to attempt to
eliminate in programs.

%Goal/objective of paper
With modern advances in program analysis, it is now possible to explore a
variety of different programs in a search space and quickly verify them to
synthesize programs that meet a specification. From this principle, a buggy C
program can be seen similarly to one of these sketches, except for the fact that
often times, the location of the bug is unknown to the programmer. Therefore,
the goal of this project is to use an automated method to eliminate these bugs
in programs. This paper discusses the design and implementation of
an algorithm to make automated repairs to simple bugs in C programs using the
LLVM infrastructure and the SeaHorn verifier.

\subsection{Summary of Previous Work}
There have been several works on program repair, including varieties
that are mutation based and those that are not.The approach for this
implementation is based on the paper ``Sound and Complete
Mutation-Based Program Repair'' by Rothenberg and
Grumberg. \cite{rothenberg2016sound}
The paper mentions many previous approaches for program repair, many of which
are quite robust, and not all of which are mutation-based, or make small,
iterative changes to the program in sets, verifying them as
needed. Additionally, as of yet, there is no publicly available implementation
of their algorithm, so it could be seen as quite an advantage to have one.

\subsection{Summary of Challenges}
Some of the main challenges of this approach is the size of the search space of
mutated programs, effective ways to enumerate mutations, appropriate operations
to replace mutations with, and the order to apply and test sets of mutations
with. Therefore, all of these aspects were carefully considered when
implementing the algorithm as outlined by Rothenberg and Grumberg, from which I
ended up differing somewhat from the related work in its approach.

\section{Problem Background and Related Work}
To address the problem of repairing buggy programs, there have been
several attempts to create software packages that correctly repair
programs. This section will contain a review of the previous approaches to
program repair, explaining which aspects are relevant to the project
and why aspects of some of the previous approaches were not used in
meeting the goal of the project. I will provide a brief summary of the
main paper that the project is based on and how many of principles and
the novel approach addressed in the paper were used in the project.

In Rothenberg ang Grumberg's paper, they mention several different
previous approaches to program repair, beginning with software
packages, many proprietary, that take various approaches to
repair. Though much of this software does not follow quite the same
approach as the one outlined in this paper, many of the programs seek
to acheive the same goal and provide useful insight into the task of
automatically debugging a program could be done. Therefore, to
understand the approach of this project, it could be useful to, as
Rothberg and Grumberg did in their paper, briefly summarize the
functionality of some of these software packages and explain their
approaches to solving the problem.

Many of the programs, such as GenProg and TrpAutoRepair follow
a ``generate and validate'' approach, learning from common programming
mistakes by using data from successful human repairs and using them to
fix possible errors in a program. \cite{rothenberg2016sound}. For
example, the
GenProg method uses a variant of genetic programming, in other words,
a program that ``retains required functionality but is not susceptible
to a given defect''\cite{le2012genprog}. However, this is mostly out
of the scope of this project, as this automated debugger is more meant
for smaller projects that are being worked on by a single person, such
as very simple programs written in introductory computer science
courses. Though there is a large number of student submissions, it is
unlikely that the iterative changes that resolve bugs in the code
exist to be learned from.

The paper also briefly mentions some other trends in program
synthesis, such as symbolic execution and synthesis. For example, the
SemFix method used synthesis and semantic analysis on programs and
followed the genetic method, boasting a higher-success
rate and higher speed than genetic-based program repair. This method
used fault isolation, or examining one bug at a time from a list of
suspicious statements, statement-level specification inference, or
turning expressions into non-deterministic expression to allow the
creation of output that would result in a test passing. Finally, the
program used synthesis to create expressions that conform to the
specification of the program. \cite{wei2010automated}. In this project
however, rather than using semantic analysis, lexical analysis is used
in the mutator module to break down operations that can potentially
replaced. Rather than using synthesis, which can be an expensive
operation, sets of mutations are explored until a set of mutations is
found that when applied to the program, creates a verifiably correct
result.

\subsection {Predecessors in Mutation-Based Program Repair}
% Most closely related work
One of the closest predecessors, however, for this approach is the
paper ``Automated error localization and correction for imperative
programs'' by Robert Könighofer and Roderick
Bloem. \cite{konighofer2011automated}
The paper describes a method from which Rothenberg and Grumberg's
approach, and thus the approach implemented in this project, certainly
stems. The method takes an incorrect program and a corresponding
specification, provided through assertions as inputs, and uses error
localizatoin methods and an SMT solver to handle incorrect
expressions, correct them by repairing the program, and verifying the
results. The key difference, however, between this approach and the
one indicated in \cite{rothenberg2016sound} is that "they use program analysis based on a finite number of
inputs each time, while we use incremental SMT solving that allows reuse of
information." Indeed, this approach employs the usage of more
information than the other approach does.

% Discuss rest of paper in detail....
\begin{figure}
   \includegraphics[width=1.0\textwidth]{soundflow.png}
   \caption{A diagram of the steps to the approach provided by Rethenberg and
Grumberg in ``Sound and Complete Mutation-Based Program Repair''.}
\label{fig:soundchart}
\end{figure}
In contrast with the previous works, \cite {rothenberg2016sound}
describes the process of repairing a program in tremendous detail. The
mutation is split into several phases, a diagram of which is included in Figure
\ref{fig:soundchart}:

\begin{itemize}
  \item
First, the translation phase
starts out with the original program, and converts it to an SSA, or
single-static assignment form. In this form, each identifier, or
variable in the program, is only assigned once. So, for example, a
variable, $ x $ that is assigned n times will be converted to
$ x_1, x_2, ... x_n $ in the static single assignment form. This
form is very useful for tracking how a variable changes throughout the
program. Mutable state in variables often makes programs more
difficult to reason about, and especially to debug, because of the
fact that the value changes throughout the process, especially if
there are side-effects. Static single assignment eliminates many of
these complications by dividing one variable into many that do not
change.
\item
  Next is the mutation phase. Before even making mutations, it is
  important to delimit which parts of the program can be mutated, and
  which can not. Therefore, the paper discusses dividing the program
  into two sets of statements, $ S_{soft}$ and $S_{hard}$ , where
  $S_{soft}$ indicates parts of the program that can be mutated, such
  as the right hand sides of assignments, and other statements, and $
  S_{hard} $ indicates parts of the program that cannot be mutated,
  such as assertions, which act as a specification for the program
  behavior, and function signatures, which are expected to stay the
  same throughout this process. This is remarkably similar to the way
  that lines are identified to have aspects that can be mutated in the
  enumerator module, as only lines with the appropriate criteria (i.e
  not preprocessor directives, comments, function headers, assertions,
  or other parts of the program that would not be part of $ S_{soft}$ and
  operators that can be mutated are considered. Once these lines are
  identified, all possible sets of mutations are found.
\item
  Once the program is divided into these two sets, the program goes
  through an algorithm that iterates through the possible variables in
  sets. A flowchart of this algorithm is included in % TODO put here.
  In summary, the program find all minimal sets of repairs, verifying
  them and adding them to a list of repairs. The key difference in the
  algorithm that is implemented in the paper and the one that was
  implemented in the project is that the paper is complete, meaning
  that it returns all possible sets of mutations, and are minimal,
  meaning that only minimal sets of mutations are returned. The
  algorithm implemented in the approach for this project returns a
  minimal set of mutations that is verified rather than finding all
  possible sets because of the simple nature of the bugs. This makes
  it more likely that a reasonable set, as would be intended by the
  programmer, is appplied.

  The repair phase applies the set of mutations found in the last phase
  and applies them to the candidate program. One the set of mutations
  is applied to the program, the progrm is then verified, From the
  paper, it is recommended that the repair unit be passed as a set of
  SMT constraints to a solver. However, in this project, it is not
  implemented this way, but rather by using a verifier called SeaHorn,
  which uses the Z3 solver as a backend
  \cite{gurfinkel2015seahorn}. This allows the mutation module to
  focus more on enumerating mutations and calculating sets of
  mutations rather than on the process of verification. 
\end{itemize}

SeaHorn is ``an LLVM-based framework for verification of safety
properties of programs.''\cite{gurfinkel2015seahorn} 
The SeaHorn verifier proved to be essential to this project, using SMT
constraints to verify the correctness of a program with appropriate
assertions. Programs are put into the verifier to gauge the need for,
and later the successfulness of mutation. It is through the verifier
that the correctness of the sets of mutations can be ensured.


\section{Approach}
\subsection{Main idea of approach}
The key idea here is to use some of the principles discussed in the
related work, but to keep it quite simple and aim at fixing very
simple bugs. In other words, this involves bugs of the very sort
that appear in the first stages of development, mainly mixed up
binary operators and asssignments. Grumberg and Rotherbenberg's
paper divides the process of mutation-based program repair into four
stages, the translation phase, tht mutation phase, the repair phase,
and the output of all minimal repairs. The implementation of these
phases, however, is not clearly specified outside of the bounds of the
description. This is likely intentional: there are many ways to
implement the algorithm, and each implementation has its advantages
and disadvantages. However, for the purpose of accomplishing the goals
of this project, the translation to SSA was done using the LLVM IR to
analyze the program and enumerate possible, and the mutations
themselves were done with a program that defines each mutation using
algebraic data types, makes changes to the source
file after outputting the minimal repairs, saving them to a separate
file with the bugs eliminated.

\subsection{Overview of algorithm}
\begin{figure}
   \includegraphics[width=1.0\textwidth]{architectureflowchart.png}
   \caption{A flowchart of the algorithm implemented in this project.}
   \label{fig:projectflow}
\end{figure}
This section will give a brief overview of the algorithm implemented
in the project, detailing each step at a high level. The accompanying
flowchart (Figure \ref{fig:projectflow}) provides an illustration of the flow of
this process.
\subsection{Candidate Programs}
\lstinputlisting[language=C]{./sum_to_bad.c}
In the first step of the process, the programmer must write a program
that contains assertions to detail the constraints of the program
behavior. This is achieved by using the set of assertions contained in
the SeaHorn libraries, like \texttt{sassert()}. If these assertions
are inserted into the program, the bugs are in the domain of bugs that
can be enumerated in the enumertor and repaired in the mutator module,
then the program will continue in the pipeline. An example of a simple
C program that meets these qualifications is attached above, as this
program was used to test the functionality of the tool.

\subsection{Enumerator and LLVM Pass}
The next step of the process is the enumerator, which is implemented
as part of an LLVM pass. In this module, the \texttt{clang} compiler
compiles the C source code to LLVM, and intermediate representation
that takes the form of static single assignment, similar to the source
to source translation referred to in
\cite{rothenberg2016sound}.
LLVM is an incredibly powerful
infrastructure intended for use in compilers. It provides an
assembly-like, intermediate representation of programs that allows for
the usage of a second stage of the compiler that can compile to a wide
variety of architectures, making it useful for a wide variety of
different computer systems. This is especially beneficial for C
programs, for which the usage of LLVM could be immensely helpful for
portability. LLVM passes are normally used to make optimizations on
programs, but here they are used to analyze the program and store
mutations. Through the pass, the different mutations that can be made are then
encoded and stored.

\subsection{Repair Module}
Once mutations are stored, the mutator module makes a set of all
possible combinations of mutations and tries them in the order of
number of mutations. After applying mutations, the modified program is
then verified. If a set of mutations creates a correct program, then
the set of mutations and the modified program are returned. This
indicates success. If not,
then the program continues to try to find a correct set. After running
out of possible mutations, the program may fail and return no
mutations, being unsuccessful in repairing the program.
\section{Implementation}
\subsection{Implementation of LLVM Pass for Mutations}
The tutorial ``LLVM' for Grad Students''
\cite{sampson2015llvm}
proved to be immensely helpful in understanding LLVM passes. From this
article, those interested in creating passes can come to understand
that the LLVM API allows for the usage of iterators over different
parts of the control flow of a program, such as basic blocks,
functions, and individual instructions. In the LLVM API itself,
instructions are encoded into various different classes of
instructions. Therefore, in the LLVM pass, mutations are stored by
switching on each instruction in a block and identifying it as one of
the target instructions for a mutation denoted in the following set of
enumerations, a superset of which appears in the repair module:
\begin{verbatim}
BinaryOp | ADD | SUB | MUL | DIV | AND | OR | SHIFTL | SHIFTR | EQEQ |
MOD
\end{verbatim}

In order to store mutations, a separate module was created called
\texttt{MutationStore}, which defines a type called \texttt{Mutation},
a pair of an iterator containing the locations of the instructions,
and the operator the replace in all instances of that
instruction. This accomodates for instructions that appear in multiple
places in the code, and allows all of the line numbers to be
retrieved. This way, all mutations are also unique and the different
possible operations are separate mutations. At the end of the pass,
the module outputs all of the possible mutations to the repair and
mutator, which then creates all of the combinations of mutations and
attempts to apply them to the program.

\subsection{Challenges of LLVM Pass Implementation}
The implementation of the LLVM Pass introduced many challenges, many
of which were avoided by doing more work in the repair module written
in Haskell rather than C++. At first, I attempted to both find all of
the possible mutations in the LLVM pass and try to apply them as I
make mutations on the LLVM bytecode, but this proved to be a
complicated process. Additionally, the SeaHorn verifier works less
reliably on LLVM bytecode that was not created through the first stage
of the SeaHorn pipeline, so this was abandoned in favor of only
getting the possible mutations in the pass, but not attempting to
apply them. Additionally, it was difficult to implement the mutation
of some operators, such as incrementing, decrementing, and assignments
with a secondary operator such as \texttt{+=} and \texttt{-=}, so
these were also left to the repair module.
\pagebreak

\subsection{Implementation of Repair Module}
Once enumerations are saved by the LLVM Pass, the repair module takes
the source program and looks for more mutations to make on top of
those suggested by the pass. The programmar can run the repair module
by passing the original source file to the script. The enumeration of
different types of mutations was done using algebraic datatypes in
Haskell. This allows for the separation of $ S_{soft}$ and $S_{hard}$
once more, while still looking for more operators than were observed
in the LLVM pass.
Below is the enumeration of each type of operator as written in this module:
\begin{verbatim}
data BinaryOp
  = PLUS | MINUS | AND| OR | SHIFTL | SHIFTR | EQEQ | MUL | DIV| MOD
  deriving Enum

data UnaryOp = INC | DEC 
  deriving Enum

data AssignmentOp
  = PLUSEQ | MINUSEQ
  deriving Enum

data Operation
  = Data String | BinOp BinaryOp| UnOp UnaryOp | AssnOp AssignmentOp
\end{verbatim}

Once the string program is passed to this module, it becomes list of
these enumerations, either a \texttt{Data} string or one of the
operator types. Subsequently, the list is passed to a function that
finds all of the possible combinations, but with a catch: operations
can only be replaced with other operations in the same subtype. For
example, unary operations with other unary operations, assignments
with other kinds of assignments, and binary operations with other
binary operations. This is so that there is a minimal number of
nonsensical mutations that are considered. Many of these higher-level
string mutations may cause compiler errors in the pipeline, in which
case they are simply ignored and the script moves on to a different
set of mutations.

After all sets of mutations are found, they are applied in order of
number of mutations and passed to the SeaHorn verifier pipeline
through execution of the \texttt{sea pf} script. If the script returns
unsat, then the number of mutations applied, the number of sets of
mutations before finding the result, and the corrected program are
outputted. If a solution can not be found, a message indicating that
mutation failed is shown to the user.

\subsection{Challenges of Repair Module Implementation}
The main challenges in creating the repair module were the design of
the different enumerations, keeping track of the size of the number of
combinations throughout the process, and the means of implementing
this. Because creating enumerations can be expressed very clearly and
efficiently with Haskell, and the type safety of the language lends to
fewer errors, this was the language chosen for the script,
and it seems to have served its purpose very well. The set of
mutations that can be made is now extendable with only a few changes
to the program. Additionally, calling the verifier nearly posed some
of the same challenges as with the LLVM pass, but executing external
programs was done by writing the c program to a file, passing that to
the verifier pipeline as an argument, and interpreting the result
with the script.


\section{Conclusions and Future Work}
Overall, the tool is able to replace simple bugs for binary operations
in a decent amount of time, enough to be helpful in the early stages of
development. Additionally, the quantity of bugs that can be repaired
is a small subset, but suffiencient enough to make some
progress. Because of how the program was written, it can
also be extended to more bugs in the future.


However, there is still a lot of work to be done. This section will
address some ways that the tool could possibly be improved and suggest
how theycould possibly be done. Unfortunately, because of time
constraints, the limited scope of the project,and the complexity of
some of these problems, I was not able to achieve progress towards
these things, but they are certainly worth mentioning and addressing
as future work.

\subsection {Priority}
When creating sets of mutations, because this is not a complete
approach, only a working set of mutations needs to found. This means
that if a set of mutations that satisfies the conditions is found, it
can quickly be returned. However, some operations are more likely to
be confused with others. For example, if the program has a -, sets
containing + in place of that minus should likely be higher priority
than sets containing other binary operations.

\subsection{Learning}
The concept of which operations should be given priority also begs the
question: how are operations that recieve higher priority actually
determined by the module? Of course, these can be hard-coded based on
common sense assumptions about most problems, but that can only
optimize the repair to a certain extent. To truly be highly efficient,
the module must also be learning from other bugs to organize the
priority as well. This could prove to be a very interesting task to
take up for those interested in the intersection of machine learning,
program synthesis, and the analysis of program structure.

\subsection{Parallelism}
In addition to learning and priority, parallelism could be used very
effectively in the mutator module to speed up performance. Because
different calls to the verifier and applying mutations to the program
are independent from each other, explicit use of parallel computing
could present as a very good extension to the module.

\subsection{Pruning and Eliminating Redundancies}
Pruning and eliminating redundancies are discussed to some extent in
\cite{rothenberg2016sound}, but the approach taken by this project
does not include it, which also likely slows it down. For example,
eliminating unreasonable sets of mutations, or leaving them last in
priority, could give a significant speed up. Additionally, removing
sets of mutations that could be reasoned about to produce the same
result as each other could also be an important way to provide speedup.

\subsection{Data Structures}
Other than speeding up the existing algorithm, it is also possible
that modifications to the algorithm itself could be made. Assertions
can sometimes have variables that they are dependent on, so the
dependencies of an assertion could be found, and thus, only the
intersection of the set of statements 
that the assertions are dependent on and $ S_{soft}$ could be
considered necessary candidates for mutation. This could likely be
done recursively by using a tree 

\section{Ethics}

I pledge my honor that this paper represents my own work in accordance with
University regulations.

\section {Acknowledgements}
\pagebreak
\bstctlcite{bstctl:etal, bstctl:nodash, bstctl:simpurl}
\bibliographystyle{IEEEtranS}
\bibliography{references}

\end{document}

