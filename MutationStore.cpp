/*********************************************************************** 
 * MutationStore.cpp
 * Author: Rod Eric Joseph
 * Description: Implementation of MutationStore class.
 **********************************************************************/

#include <iostream> 
#include <list> 
#include <iterator>
#include <utility> 
#include "MutationStore.h"

using namespace llvm; 
class MutationStore
{
public:
  std::vector <Mutation> members;

  std::vector <std::vector <Mutation>> combinations(int n)
  {
    vector <vector<Mutation>> v;
    if(n==1)
      {
	for(int i = 0; i < members.size(); i++)
	  {
	    std::vector <Mutation> v0;
	    v0.push_back(members[i]);
	    v.push_back(v0);
	  }
      }
    return v;
  }
  
  // Add mutation to the list of mutations (make Mutation from pair).
  void add(iterator_range<use_iterator> *uses, BinaryOps *opcode)
  {
    m = make_pair(uses, opcode);
    members.push_front(m);
    return;
  }

  void apply(iterator_range<use_iterator> *uses, BinaryOps *opcode)
  {
    // Everywhere the old instruction was used as an operand, use our
    // new instruction instead.
    for (auto &U : op->uses())
    {
      User *user = U.getUser();  // A User is anything with operands.
      user->setOperand(U.getOperandNo(), new_op);
    }
    return;
  }
}; 
  
int main() {
  return 0;
} 
