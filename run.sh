# put this script to the seahorn directory

F=$(sed 's/.\{2\}$//' <<< "$1")
FBC=$F".bc"
FL=$F".log"
FT=$F".txt"

./build/run/bin/clang -c -emit-llvm -D__SEAHORN__ -fgnu89-inline -m32 -I/Users/grigory/Dev/seahorn/include $1 -o $FBC

./build/run/bin/llvm-dis $FBC -o $FT # for human-readable output

./build/tools/seapp/seapp --horn-inline-all --strip-extern=false --promote-assumptions=false --kill-vaarg=true -o $FBC $FBC
./build/tools/seapp/seapp --horn-mixed-sem  -o $FBC $FBC
./build/bin/seaopt -f -funit-at-a-time  --enable-indvar=false --enable-loop-idiom=false --enable-nondet-init=false --unroll-threshold=150 --disable-loop-vectorization=true --disable-slp-vectorization=true --vectorize-slp-aggressive=false -o $FBC $FBC

echo Verifying $1
./build/tools/seahorn/seahorn --keep-shadows=true --horn-solve -horn-inter-proc -horn-sem-lvl=mem --horn-step=flarge $FBC > $FL 2>&1
grepped=$(cat $FL | grep 'sat\|unsat\|Unsupported')

if [ $grepped == 'unsat' ]
then
  echo Result: successful
else
  echo Result: failed
fi

rm $FL $FBC
